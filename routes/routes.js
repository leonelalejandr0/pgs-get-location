const express = require('express');
const LocationNodeCtrl = require('../controllers/LocationNode.controller');
const Tenant = require('pgs-tenant-middleware');


let app = express();

app.use(require('./middlewares/log-request.middleware'));
app.use(Tenant.validar);

let router = express.Router();

router.get('/get/:regionReference/:cityReference/:communeReference', LocationNodeCtrl.getAll);

app.use(router);


module.exports = app;